**Join The World (Version 1.0)** Translate By: Mary Righetto

This work is licensed under a Creative Commons Attribution NonCommercial ShareAlike 4.0 International License.

**EQUIPMENT***

- A Billboard
- Ten Pawns Green Resources (Environmental Sustainability)
- Six pawns Blue Resources (Economy) 
- Ten Pawns Red Resources (Social Aspects).
- Twelve White Pawns (Hope)
- One Pawn Progress (superior in dimension compare to the others) Current Society 
- One Pawn Progress (superior in dimension compare to the others) Free Society 
- 40 Cards Free Society (light)
- 40 Cards Current Society (dark)

*Colors may change according to what you have at home to use as pawns

- **Currently the manuals are in: English, Italian, Spanish and Russian**
- **Currently the cards are in: Italian**

The cards are characterized by a digit called Factor of Development that can be positive or negative and by a
color marker indicating the Category that is the color of the pawns to be positioned above the card, taking them form the game.

**HOW TO ARRANGE THE TABLE AND PREPARATION AND GOALS OF THE GAME**

Place the deck of 80 cards, after mixing, in the space provided 'DECK'.
In the space 'GRAVEYARD' will be placed all the discarded cards during the game.

Decide who should perform the first action, establish a dialogue between the players and prepare to save the world 
from decay! Download and print the game billboard and place both societies' pawns at the center, in the YELLOW colored box.

Each player will begin the game with SEVEN cards available.

Each player will reserve a space, between himself and the billboard, for his own Laboratory of Ideas where he 
MUST have three positive cards of different colors (at each turn the player can play only ONE card) and their 
resources (the color pawns) which number is equal to the number on the card (in the example below we should put 
above each card, respectively: 3 green pawns, 5 red pawns and 3 blue pawns). 

In case there are fewer resources available to the number on the card the player will have to "settle" for such resources. If there are no resources of a specific color, the color corresponding to that card will not be played 
until the "availability" of new resources. When a player manages to accumulate 3 cards in his Laboratory a positive 
card present in your hand can be played on the board at the cost of ONE pawn Environmental Sustainability, ONE  pawn Economy and ONE pawn Social Aspects. When a card in your Laboratory ends the resources, it is eliminated from the game.

It can happen that a player does NOT have Cards Resources Positive immediately:
In that case he will be forced to play a negative card, thereby increasing the points Resignation in the billboard.
Once played, the Negative card is eliminated from the game (ending in GRAVEYARD space face down).

**The game ends when:**
- The pawn Free Society or the one Current Society arrives at the end of its route. 
- In case the deck runs out, the society with the highest (progress pawn) score wins.

**EXECUTION**
- Mix the cards, put them in DECK box, in the billboard, and alternately each player takes SEVEN cards.
- Play a Resource Card + (positive) to start developing a Technology or to analyze a Current of Thought in the Laboratory of Ideas if there aren't other cards of the same color, this is because you hsve to analyze all the
Aspects of the Society and give credibility to the people. In the game, the resource card takes many pieces of the same color equal to its value if available or those that remain.
- Use two resources of a card in your own Laboratory of Ideas to fish or draw a card:
During the game, it can happen that a player with collaborative spirit exclaims "Who needs to draw?" in most cases the previous player had warned the team not to possess positive cards in hand and does not have resources in the laboratory
- Use three resources, one per color, to play a positive card in hand on the billboard and then inform people 
about a particular technology or philosophy of thought. At this point the player has achieved credibility and 
has proven to have analyzed well the Social Aspects, Economy and Environmental Sustainability. The Free Society
 increases his own “hope” (white pawns, to put on the LIGHT BLUE numbers) of many points as the value of the card played. If the Hope Value reaches six, the Free Spciety pawn moves one LIGHT BLUE box and the white pawns are removed.
- Play a Card - (minus) on the billboard, the society's dogmas are respected and undisputed, the Current 
Society increases the “resignation” (white pawns, to put on RED numbers) of many points as the value of the 
card played. If the Resignation Value reaches six, the Current Society pawn moves one RED box and the white 
pawns are removed.
- If the player has no cards in hand he misses the turn.